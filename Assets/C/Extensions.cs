﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public static class Extensions {
	// for colliders 

	//
	public static Rigidbody RBODY(this GameObject g){
		return g.GetComponent<Rigidbody>();
	}


	public static Image IMG(this GameObject g){
		return g.GetComponent<Image>();
	}
	public static Slider Slidr(this GameObject g){
		return g.GetComponent<Slider>();
	}
	public static void SetText(this GameObject g,string str){
		g.GetComponent<Text>().text=str;
	}
	public static void SetText(this GameObject g,int obj){
		g.GetComponent<Text>().text=obj.ToString();
	}
	public static void SetText(this GameObject g,float obj){
		g.GetComponent<Text>().text=obj.ToString();
	}
	public static Text TXT(this GameObject g){
		return g.GetComponent<Text>();
	}
	public static void SetImageFill(this GameObject g,float valu){
		g.GetComponent<Image>().fillAmount=valu;
	}

	public static void SetAlfa(this GameObject g,float alfavalue){
		Color clr=g.GetComponent<Image>().color;
		clr.a=alfavalue;
		g.GetComponent<Image>().color=clr;
	}

	public static Vector2 RectSize(this GameObject g){
		Vector3[]v=new Vector3[4];
		g.GetComponent<RectTransform>().GetLocalCorners(v);
		return new Vector2(Vector3.Distance(v[0],v[3]),Vector3.Distance(v[0],v[1]));
	}
	/// <summary>
	/// Returns Rect Transform
	/// </summary>
	public static RectTransform RectT(this GameObject g){
		return g.GetComponent<RectTransform>();
	}
	public static Vector2 RectSize(this RectTransform g){
		Vector3[]v=new Vector3[4];
		g.GetLocalCorners(v);
		return new Vector2(Vector3.Distance(v[0],v[3]),Vector3.Distance(v[0],v[1]));
	}
	public static Vector2 RectSizeWorldPos(this GameObject g){
		Vector3[]v=new Vector3[4];
		g.GetComponent<RectTransform>().GetWorldCorners(v);
		return new Vector2(Vector3.Distance(v[0],v[3]),Vector3.Distance(v[0],v[1]));
	}

	public static Button.ButtonClickedEvent OnCLICK(this GameObject g){
		return g.GetComponent<Button>().onClick;
	}
	public static Button BTN(this GameObject g){
		return g.GetComponent<Button>();
	}
	public static GameObject CreateFromPrefabe(this GameObject g,GameObject objParent,bool setActive){
		GameObject gg=(GameObject)MonoBehaviour.Instantiate(g);
		if(objParent!=null){
			gg.transform.SetParent(objParent.transform);
		}
		gg.transform.localScale=Vector3.one;
		gg.transform.localEulerAngles=Vector3.zero;
		gg.transform.localPosition=Vector3.zero;
		if(setActive){
			gg.SetActive(true);
		}
		return gg;
	}


	public static void DontDetroy(this GameObject g){
		MonoBehaviour.DontDestroyOnLoad(g);
	}



	public static void PauseUnpauseAudio(this GameObject g,bool pause){
		AudioSource tmp=g.GetComponent<AudioSource>();
		if(pause){
			if(tmp.isPlaying){
				tmp.Pause();
			}
		}
		else{
			if(!tmp.isPlaying){
				tmp.UnPause();
			}
		}
	}


	public static void StopAudio(this GameObject g){
		//if(SettingsScreen.IsSoundMute==0){
		AudioSource ap=g.GetComponent<AudioSource>();
		if(ap!=null){
			if(ap.isPlaying){
				ap.Stop();
			}
		}

		//}
	}

	public static bool IsSameCollider(this GameObject g,Collider col){
		return g.GetComponent<Collider>().Equals(col);
	}

	public static void AnimationTrigger(this GameObject g,string triggerString){
		g.GetComponent<Animator>().SetTrigger(triggerString);
	}

	public static int RandomInt(this MonoBehaviour m,int min,int max){
		return Random.Range(min,max);
	}
	public static void SetPos(this GameObject g,Vector3 pos){
		g.transform.position=pos;
	}
	public static void SetPos(this GameObject g,GameObject posobj){
		g.transform.position=posobj.transform.position;
	}
	public static Vector3 GetPos(this GameObject g){return g.transform.position;}

	public static bool MovedToLocalPos(this Transform t,Transform target,float speed){
		t.localPosition=Vector3.MoveTowards(t.localPosition,target.localPosition,Time.deltaTime*speed);
		return t.localPosition==target.localPosition;
	}
	public static bool MovedToLocalPos(this GameObject t,GameObject target,float speed){
		t.transform.localPosition=Vector3.MoveTowards(t.transform.localPosition,target.transform.localPosition,Time.deltaTime*speed);
		return t.transform.localPosition==target.transform.localPosition;
	}
	public static bool MovedToLocalAngle(this GameObject t,GameObject target,float speed){
		Vector3 v=t.transform.localEulerAngles;
		Vector3 tangle=target.transform.localEulerAngles;
		v.x=Mathf.MoveTowardsAngle(v.x,tangle.x,Time.deltaTime*speed);
		v.y=Mathf.MoveTowardsAngle(v.y,tangle.y,Time.deltaTime*speed);
		v.z=Mathf.MoveTowardsAngle(v.z,tangle.z,Time.deltaTime*speed);
		t.transform.localEulerAngles=v;
		return v==tangle;
	}
	public static bool MovedToLocalPos(this GameObject t,Vector3 target,float speed){
		t.transform.localPosition=Vector3.MoveTowards(t.transform.localPosition,target,Time.deltaTime*speed);
		return t.transform.localPosition==target;
	}
	public static bool MovedToPos(this GameObject g,Vector3 targetpos,float speed){
		g.transform.position=Vector3.MoveTowards(g.transform.position,targetpos,Time.deltaTime*speed);
		return g.transform.position==targetpos;
	}
	public static void SmoothlyLookAt(this Transform t,Transform target,float speed){
		Quaternion rotation = Quaternion.LookRotation(target.position - t.position);
		t.rotation = Quaternion.Slerp(t.rotation, rotation, Time.deltaTime * speed);
	}


	public static Vector3 ScreenLeft(this Camera cam,float distanceFromCam){
		//get{ 
			return cam.ViewportToWorldPoint (new Vector3(0.0f,0.0f,distanceFromCam));
		//}
	}
	
	
	public static LERP LerpAndJumpRectItem(this GameObject g,Vector2 tpos,Vector2 jump1,Vector2 jump2,float targetTime,float jump1Time,float jump2Time){
		LERP tmp=g.AddComponent<LERP>();
		tmp.StartCoroutine(tmp.LerpAndJumpEnum(tpos,jump1,jump2,targetTime,jump1Time,jump2Time));
		return tmp;
	}
		
}

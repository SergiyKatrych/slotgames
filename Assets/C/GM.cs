﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

	// Use this for initialization
	public GameObject soundManager;
	void Awake () {
		Application.targetFrameRate=30;
		if(SoundManager.instance==null)
		{
			soundManager.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
[System.Serializable]
public struct Padding{
	public float left;
	public float right;
	public float top;
	public float bottom;
}


using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GamePlay : MonoBehaviour
{
	public static GamePlay instance
	{
		get { return obj.GetComponent<GamePlay>(); }
	}

	private static GameObject obj;


	[SerializeField] Camera cam;
	[SerializeField] GameObject homebtn;
	[SerializeField] GameObject congratesScreenBg;
	[SerializeField] GameObject mainSpinButton;
	[SerializeField] GameObject congratesScreen;
	[SerializeField] GameObject winIcon;
	[SerializeField] RectTransform reelsArea;
	[SerializeField] GameObject reelItemPref;
	[SerializeField] GameObject reel1, reel2, reel3;
	[SerializeField] GameObject winIndicatorBg;
	Sprite winItemSprite;

	public List<ReelItemInfo> reelVariables;

	[SerializeField] float topPadding = 0.0f;
	[SerializeField] float bottomPadding = 0.0f;
	[SerializeField] float space = 0.0f;

	[SerializeField] int rowCount = 5;
	public int oneSuredWinningIndex = -1;
	public int suredWinnVariableIndex = -1;
	public int spinCounter = 0;
	private int totalSpinCount = 0;
	public int stageCounter = 0;

	private List<int> item_VariableIndexes;
	private List<int> item_variableIndexs2;
	private List<int> winningCombinationIndexes;
	private List<int> winningCombinationIndexes2;

	public List<ReelItemInfo?> winningList;


	private static Random _random;

	void Awake()
	{
		obj = gameObject;
	}

	private void InitWinningList()
	{
		List<ReelItemInfo> onlyWinItemsList = new List<ReelItemInfo>();
		onlyWinItemsList.Add(reelVariables[1]);
		onlyWinItemsList.Add(reelVariables[1]);
		
		onlyWinItemsList.Add(reelVariables[2]);
		onlyWinItemsList.Add(reelVariables[2]);
		
		onlyWinItemsList.Add(reelVariables[3]);
		onlyWinItemsList.Add(reelVariables[3]);
		
		onlyWinItemsList.Add(reelVariables[4]);
		onlyWinItemsList.Add(reelVariables[4]);

		for (int i = 0; i < 92; i++)
		{
			onlyWinItemsList.Add(reelVariables[0]);
		}

		Shuffle(onlyWinItemsList);

		winningList = new List<ReelItemInfo?>(300);

		string list = "";
		for (int i = 0; i < 100; i++)
		{
			int winningIndex = Random.Range(0, 3);

			for (int j = 0; j < 3; j++) {

				winningList.Add(null);
				if (j == winningIndex)
				{
					int itemIndexToRemove = Random.Range(0, onlyWinItemsList.Count); // extra shufling
					winningList[winningList.Count-1] = onlyWinItemsList[itemIndexToRemove];
					onlyWinItemsList.RemoveAt(itemIndexToRemove);

					list += "\n " + (i*3+j) + ": " + winningList[winningList.Count-1].Value.id;
				}
				else
				{

					list += "\n " + (i*3+j) + ": " + winningList[winningList.Count-1];
				}
			}



		}

//		int freeSpinCountBeforeFirstWin = Random.Range(0, 3);
//		for (int i = 0; i < freeSpinCountBeforeFirstWin; i++)
//		{
//			winningList.Insert(0, null);	
//		}
			
	Debug.Log(list);
	}


	public void Shuffle<T>(List<T> list)
	{
		int n = list.Count;
		while (n > 1)
		{
			n--;
			int k = Random.Range(0, n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}

	void Start()
	{
		item_VariableIndexes = new List<int>();

		for (int i = 0; i < reelVariables.Count; i++)
		{
			item_VariableIndexes.Add(i);
		}

		oneSuredWinningIndex = Random.Range(0, 3);

		suredWinnVariableIndex = RandomVariableIndex;

		totalSpinCount = 0;
		InitWinningList();

		CreatReel();
	}

	public int GetWinningCombinationIndex
	{
		get
		{
			if (winningCombinationIndexes == null)
				winningCombinationIndexes = new List<int>();

			if (winningCombinationIndexes.Count == 0)
			{
				for (int i = 0; i < 92; i++)
					winningCombinationIndexes.Add(0);

				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 1);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 2);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 3);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 4);

				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 3);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 2);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 4);
				winningCombinationIndexes.Insert(Random.Range(0, (winningCombinationIndexes.Count - 1)), 1);
			}

			int itemIndexToReturn = Random.Range(0, winningCombinationIndexes.Count);
			int itemToReturn = winningCombinationIndexes[itemIndexToReturn];
			winningCombinationIndexes.RemoveAt(itemIndexToReturn);
			return itemToReturn;
		}
	}

	public void CreatReel()
	{
		StartCoroutine(CreateReelEnum());
	}

	IEnumerator CreateReelEnum()
	{
		Vector2 reelSize = reelsArea.RectSize();

		float reelWidth = reel1.RectSize().x;

		float itemHeight = (reelSize.y - (topPadding + bottomPadding + (space * (rowCount - 1 - 1)))) /
		                   (float)(rowCount - 1);

		float itemWidth = reelWidth;

		Vector2 winningIndicatorBgScale = winIndicatorBg.RectT().sizeDelta;

		winningIndicatorBgScale.y = itemHeight + 10.0f;

		winIndicatorBg.RectT().sizeDelta = winningIndicatorBgScale;

		Vector2 indicatorPos = winIndicatorBg.RectT().anchoredPosition;

		indicatorPos.y = reel1.RectT().anchoredPosition.y - 5.0f;

		winIndicatorBg.RectT().anchoredPosition = indicatorPos;

		Vector2 topReelCenterPos = Vector2.zero + new Vector2(0.0f, (reelSize.y * .5f));

		Vector2 bottomReelCenter = Vector2.zero - new Vector2(0.0f, (reelSize.y * .5f));

		Reel reel = reel1.GetComponent<Reel>();
		List<int> reel1ItemListIndexes = GetReelItemIndexList(ref reel);

		reel = reel2.GetComponent<Reel>();
		List<int> reel2ItemListIndexes = GetReelItemIndexList(ref reel);

		reel = reel3.GetComponent<Reel>();
		List<int> reel3ItemListIndexes = GetReelItemIndexList(ref reel);

		bool hasWon = (reel1ItemListIndexes[2] == reel2ItemListIndexes[2]) &&
		              (reel1ItemListIndexes[2] == reel3ItemListIndexes[2]);


		if (hasWon)
		{
			while (hasWon)
			{
				reel1ItemListIndexes[2] = RandomVariableIndex;
				reel2ItemListIndexes[2] = RandomVariableIndex;
				reel3ItemListIndexes[2] = RandomVariableIndex;
				hasWon = (reel1ItemListIndexes[2] == reel2ItemListIndexes[2]) &&
				(reel1ItemListIndexes[2] == reel3ItemListIndexes[2]);
			}
		}

		reel1.GetComponent<Reel>().CreateList(itemWidth, itemHeight, topReelCenterPos, topPadding, bottomPadding, space,
			bottomReelCenter);
		reel = reel1.GetComponent<Reel>();
		FillReel(reel, ref reel1ItemListIndexes);


		yield return new WaitForSeconds(.5f);
		reel2.GetComponent<Reel>().CreateList(itemWidth, itemHeight, topReelCenterPos, topPadding, bottomPadding, space,
			bottomReelCenter);
		reel = reel2.GetComponent<Reel>();
		FillReel(reel, ref reel2ItemListIndexes);

		yield return new WaitForSeconds(.3f);
		reel3.GetComponent<Reel>().CreateList(itemWidth, itemHeight, topReelCenterPos, topPadding, bottomPadding, space,
			bottomReelCenter);
		reel = reel3.GetComponent<Reel>();
		FillReel(reel, ref reel3ItemListIndexes);
	}

	private ReelItemInfo? TryToSetWinningSituation()
	{
		ReelItemInfo? winningItem = winningList[totalSpinCount % winningList.Count];


		if (winningItem.HasValue)
		{
			var item = winningItem.GetValueOrDefault();

			reel1.GetComponent<Reel>().items[2].SetItem(item);
			reel2.GetComponent<Reel>().items[2].SetItem(item);
			reel3.GetComponent<Reel>().items[2].SetItem(item);
		}

		return winningItem;
	}


	public void GoToHome(string sceneName)
	{
		SoundManager.instance.OnHomeButtonClickSound();
		Application.LoadLevel(sceneName);
	}

	public void SpinFromCongratesScreen()
	{
		SoundManager.instance.SpinSoundPlay();
		thisIsWin = false;
		winIndicatorBg.SetActive(false);
		congratesScreen.SetActive(false);
		congratesScreenBg.SetActive(false);
		isSpinning = false;
		homebtn.BTN().interactable = false;
		StartCoroutine(StartSpin());
	}

	public void OnClickSpinButton()
	{
		if (!thisIsWin)
		{
			if (winIndicatorBg.activeSelf)
				winIndicatorBg.SetActive(false);
			if (!isSpinning)
			{
				StartCoroutine(StartSpin());
			}
		}
	}

	public static bool isSpinning = false;


	IEnumerator StartSpin()
	{
		homebtn.BTN().interactable = false;
		mainSpinButton.BTN().interactable = false;
		mainSpinButton.SetActive(true);
		SoundManager.instance.SpinSoundPlay();


		isSpinning = true;


		Reel reel = reel1.GetComponent<Reel>();
		List<int> reel1ItemListIndexes = GetReelItemIndexList(ref reel);

		reel = reel2.GetComponent<Reel>();
		List<int> reel2ItemListIndexes = GetReelItemIndexList(ref reel);

		reel = reel3.GetComponent<Reel>();
		List<int> reel3ItemListIndexes = GetReelItemIndexList(ref reel);



		// no ways to have winning combination here
		bool hasWon = (reel1ItemListIndexes[2] == reel2ItemListIndexes[2]) &&
			(reel1ItemListIndexes[2] == reel3ItemListIndexes[2]);


		if (hasWon)
		{
			while (hasWon)
			{
				reel1ItemListIndexes[2] = RandomVariableIndex;
				reel2ItemListIndexes[2] = RandomVariableIndex;
				reel3ItemListIndexes[2] = RandomVariableIndex;
				hasWon = (reel1ItemListIndexes[2] == reel2ItemListIndexes[2]) &&
					(reel1ItemListIndexes[2] == reel3ItemListIndexes[2]);
			}
		}


		reel = reel1.GetComponent<Reel>();

		FillReel(reel, ref reel1ItemListIndexes);

		reel.StartSpin(0.0f);

		yield return new WaitForSeconds(.25f);
		reel = reel2.GetComponent<Reel>();

		FillReel(reel, ref reel2ItemListIndexes);

		reel.StartSpin(0.0f);

		yield return new WaitForSeconds(.25f);
		reel = reel3.GetComponent<Reel>();

		FillReel(reel, ref reel3ItemListIndexes);

		reel.StartSpin(0.0f);



		var winnitItem = TryToSetWinningSituation();
		thisIsWin = false;

		if (winnitItem.HasValue)
		{
			var item = winnitItem.Value;
			oneSuredWinningIndex = Random.Range(0, 3);
			spinCounter = 0;
			winItemSprite = item.congratsIcon;
			thisIsWin = true;
		}
		else
		{
			spinCounter++;
		}
	}

	bool thisIsWin = false;

	public void OnFinishSpin()
	{
		totalSpinCount++;
		
		isSpinning = false;
		SoundManager.instance.StopSpinSound();
		if (thisIsWin)
		{
			SoundManager.instance.OnWinSoundPlay();
			winIndicatorBg.SetActive(true);
			StartCoroutine(CongratesScreenOpenOnWin());
		}
		else
		{
			homebtn.BTN().interactable = true;
			mainSpinButton.BTN().interactable = true;
			if (!mainSpinButton.activeSelf)
				mainSpinButton.SetActive(true);
		}
	}

	IEnumerator CongratesScreenOpenOnWin()
	{
		mainSpinButton.SetActive(false);
		winIcon.IMG().sprite = winItemSprite;
		yield return new WaitForSeconds(2.0f);
		congratesScreenBg.SetActive(true);
		SoundManager.instance.OnWinSoundStop();
		SoundManager.instance.OnCongratesSoundPlay();
		congratesScreen.SetActive(true);
		//yield return new WaitForSeconds(1.50f);
		homebtn.BTN().interactable = true;
		mainSpinButton.BTN().interactable = true;
	}


	public List<int> GetReelItemIndexList(ref Reel reel)
	{
		List<int> tmplist = new List<int>();

		for (int i = 0; i < reel.itemCount; i++)
		{
			tmplist.Add(RandomVariableIndex);
		}

		return tmplist;
	}

	public void FillReel(Reel reel, ref List<int> variableIndxList)
	{
		for (int i = 0; i < reel.itemCount; i++)
		{
			reel.items[i].SetItem(reelVariables[variableIndxList[i]]);
		}
	}

	public float winningKoef = 0.33f;
	public float winningCoffeeKoef = 0.92f;
	public float winnigNotCoffeeKoef = 0.02f;
	public System.DayOfWeek[] notWinningDaysIndex = { System.DayOfWeek.Sunday, System.DayOfWeek.Saturday };

	public int RandomVariableIndex
	{
		get
		{
			///FILL RANDOM NUMBERS
			if (item_variableIndexs2 == null)
			{
				item_variableIndexs2 = new List<int>();
			}

			if (item_variableIndexs2.Count == 0)
			{
				for (int i = 0; i < 3; i++)
				{
					for (int j = 0; j < item_VariableIndexes.Count; j++)
					{
						if (item_variableIndexs2.Count < 3)
						{
							item_variableIndexs2.Add(item_VariableIndexes[j]);
						}
						else
						{
							int randomIndexToInsertAt = Random.Range(0, item_variableIndexs2.Count);

							item_variableIndexs2.Insert(randomIndexToInsertAt, item_VariableIndexes[j]);
						}
					}
				}
			}
			/// ////////////////

			int randomIndexToReturn = Random.Range(0, item_variableIndexs2.Count);
			int itemToReturn = item_variableIndexs2[randomIndexToReturn];
			item_variableIndexs2.RemoveAt(randomIndexToReturn);

			return itemToReturn;
		}
	}

	private bool IsTodayAWinningDay()
	{
		var today = System.DateTime.Today.DayOfWeek;
		return !notWinningDaysIndex.Contains(today);
	}

	private bool IsNowAWinningTry()
	{
		return Random.Range(0, 1f) < winningKoef;
	}

	private ReelItemInfo GetWinningItem()
	{
		float result = Random.Range(0f, 1f);

		if (result <= winningCoffeeKoef)
		{
			return reelVariables[0];
		}
		else
		{
			return reelVariables[Random.Range(1, reelVariables.Count)];
		}
	}
}


[System.Serializable]
public struct ReelItemInfo
{
	public string id;
	public Sprite icon;
	public Sprite congratsIcon;
}
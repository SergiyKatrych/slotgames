﻿using UnityEngine;
using System.Collections;

public class HomeControl : MonoBehaviour {

	public GameObject soundManager;
	public GameObject playBtn;

	public void LoadGamePlay(string sceneName){
		SoundManager.instance.OnHomeButtonClickSound();
		Application.LoadLevel(sceneName);
	}



	void Awake(){
		if(SoundManager.instance==null){
			Application.targetFrameRate=30;
			soundManager.SetActive(true);
		}
	}

	void Start(){
		StartCoroutine(HomeBtnAnim());
	}

	IEnumerator HomeBtnAnim(){
		yield return new WaitForSeconds(1.50f);
		playBtn.SetActive(true);
	}
}

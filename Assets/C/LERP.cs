﻿using UnityEngine;
using System.Collections;

public class LERP : MonoBehaviour {
	[SerializeField]RectTransform targetPos;
	[SerializeField]bool useAtStart=false;
	[SerializeField]float travelTime=1.0f;
	void Start(){
		if(useAtStart){
			StartCoroutine(LerpRectEnum(targetPos,travelTime));
		}
	}

	IEnumerator LerpRectEnum(RectTransform target,float timeToFinish){
		RectTransform me=GetComponent<RectTransform>();
		Vector2 start=me.anchoredPosition;
		Vector2 end=target.anchoredPosition;
		Vector2 tmp=start;
		float time=0.0f;

		while(time<=timeToFinish){
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			yield return null;
		}
		me.anchoredPosition=end;
	}


	public IEnumerator LerpAndJumpEnum(Vector2 tpos,Vector2 jump1,Vector2 jump2,float targetTime,float jump1Time,float jump2Time){

		RectTransform me=GetComponent<RectTransform>();
		Vector2 start=me.anchoredPosition;
		Vector2 end=tpos;
		Vector2 tmp=start;
		float time=0.0f;
		float timeToFinish=targetTime;
		float demotime=0.0f;

		while(time<=timeToFinish){//to target
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			demotime+=Time.deltaTime;
			yield return null;
		}

		me.anchoredPosition=end;

		start=me.anchoredPosition;
		end=jump1;
		tmp=start;
		time=0.0f;
		timeToFinish=jump1Time;

		while(time<=timeToFinish){//to jump1
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			yield return null;
		}
		me.anchoredPosition=end;

		start=me.anchoredPosition;
		end=tpos;
		tmp=start;
		time=0.0f;
		timeToFinish=jump1Time;

		while(time<=timeToFinish){//back to target
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			yield return null;
		}
		me.anchoredPosition=end;
		
		start=me.anchoredPosition;
		end=jump2;
		tmp=start;
		time=0.0f;
		timeToFinish=jump2Time;

		while(time<=timeToFinish){//to jump2
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			yield return null;
		}
		me.anchoredPosition=end;

		start=me.anchoredPosition;
		end=tpos;
		tmp=start;
		time=0.0f;
		timeToFinish=jump2Time;
		
		while(time<=timeToFinish){//back to target
			time+=Time.deltaTime;
			float normalizedvalue=time/timeToFinish;
			tmp=Vector2.Lerp(start,end,normalizedvalue);
			me.anchoredPosition=tmp;
			yield return null;
		}
		me.anchoredPosition=end;

		Destroy(this);
	}

}

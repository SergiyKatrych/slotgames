﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Reel : MonoBehaviour {

	[HideInInspector]public ReelItemInfo info;
	[SerializeField]GameObject itemPref;
	public List<ReelItem>items;
	public int itemIndex=-1;
	//[SerializeField]
	public int itemCount=80;
	[SerializeField]bool sendMessageOnSpinFinish=false;
	public void SetValues(ReelItemInfo tmp){
		info=tmp;
		gameObject.IMG().sprite=info.icon;
	}

	public void CreateList(float w,float h,Vector2 topCenter,float top,float bottom,float space,Vector2 bottomCenter){
		Vector3[]r=new Vector3[4];

		GetComponent<RectTransform>().GetLocalCorners(r);
		for(int i=0;i<itemCount;i++){
			int j=itemCount-i-1;
			GameObject g=itemPref.CreateFromPrefabe(gameObject,true);
			g.GetComponent<RectTransform>().sizeDelta=new Vector2(w,h);
			float ypos=(topCenter.y-(top)-(i*space)-(i*h));
			float hiddenYPos=(bottomCenter.y+bottom+(j*space)+(j*h));

			if(i<(itemCount-5)){
				items.Add(g.GetComponent<ReelItem>().Init(new Vector2(0.0f,hiddenYPos),new Vector2(0.0f,ypos)));
			}
			else{
				float startHiddenPosY=(topCenter.y+(top)+h*.5f+(j*space)+(j*h));
				items.Add(g.GetComponent<ReelItem>().InitAtHiddenPosition(new Vector2(0.0f,hiddenYPos),new Vector2(0.0f,ypos),new Vector2(0.0f,startHiddenPosY)));

			}
		}
	}

	public void StartSpin(float delay){
		for(int i=0;i<itemCount;i++){
			items[i].ResetToHiddenPos();
		}

		StartCoroutine(MoveToSpinTarget(delay));
	}

	IEnumerator MoveToSpinTarget2(float delay){
		yield return new WaitForSeconds(delay);
		bool isMoving=true;

		while(isMoving){
			int cnt=0;
			for(int i=0;i<itemCount;i++){
				bool hasMoved=items[i].MoveToTargetWithLerp();
				if(!hasMoved)cnt=1;
			}
			isMoving=cnt==1;
			if(isMoving)yield return null;
		}
		if(sendMessageOnSpinFinish){
			GamePlay.instance.OnFinishSpin();
		}
	}

	IEnumerator MoveToSpinTarget(float delay){
		yield return new WaitForSeconds(delay);
		bool isMoving=true;

		for(int i=0;i<itemCount;i++){
			items[i].StartCoroutine(items[i].MoveLikeSlotReel());
		}

		while(isMoving){
			int cnt=0;
			for(int i=0;i<itemCount;i++){
				if(items[i].isSpinning){
					cnt=1;
					break;
				}
			}
			if(cnt==1){
				yield return null;
			}
			else{
				isMoving=false;
				break;
			}
		}
//		while(isMoving){
//			int cnt=0;
//			for(int i=0;i<itemCount;i++){
//				bool hasMoved=items[i].MoveToTarget();
//				if(!hasMoved)cnt=1;
//			}
//			isMoving=cnt==1;
//			if(isMoving)yield return null;
//		}

//		isMoving=true;

//		while(isMoving){
//			int cnt=0;
//			for(int i=0;i<itemCount;i++){
//				bool hasMoved=items[i].MoveToJump1();
//				if(!hasMoved)cnt=1;
//			}
//			isMoving=cnt==1;
//			if(isMoving)yield return null;
//		}
//
//		isMoving=true;
//		
//		while(isMoving){
//			int cnt=0;
//			for(int i=0;i<itemCount;i++){
//				bool hasMoved=items[i].BackToTarget();
//				if(!hasMoved)cnt=1;
//			}
//			isMoving=cnt==1;
//			if(isMoving)yield return null;
//		}
//
//
//		isMoving=true;
//		
//		while(isMoving){
//			int cnt=0;
//			for(int i=0;i<itemCount;i++){
//				bool hasMoved=items[i].MoveToJump2();
//				if(!hasMoved)cnt=1;
//			}
//			isMoving=cnt==1;
//			if(isMoving)yield return null;
//		}
//
//		isMoving=true;
//		
//		while(isMoving){
//			int cnt=0;
//			for(int i=0;i<itemCount;i++){
//				bool hasMoved=items[i].BackToTarget();
//				if(!hasMoved)cnt=1;
//			}
//			isMoving=cnt==1;
//			if(isMoving)yield return null;
//		}

		if(sendMessageOnSpinFinish){
			GamePlay.instance.OnFinishSpin();
		}
	}
}

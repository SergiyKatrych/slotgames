using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReelController : MonoBehaviour
{
	public int spinCounter = 0;
	public int OneWinningCombinationIndex = 0;
	public List<ReelItemInfo> reelData1;
	public List<Transform> Reels;
	[SerializeField] List<Vector2> StartReelAnchorPositions;

	void Start()
	{
		int randomVal = Random.Range(0, reelData1.Count);
		winningDataInfo = reelData1[randomVal];
		InitReel();
	}

	public void InitReel()
	{
		OneWinningCombinationIndex = Random.Range(0, 10);

		for (int i = 0; i < Reels.Count; i++)
		{
			int childCount = Reels[i].childCount;
			StartReelAnchorPositions.Add(Reels[i].GetComponent<RectTransform>().anchoredPosition);
			for (int j = 0; j < childCount; j++)
			{
				Reel item = Reels[i].GetChild(j).gameObject.GetComponent<Reel>();

				int randomVal = Random.Range(0, reelData1.Count);

				item.SetValues(reelData1[randomVal]);
			}
		}
	}

	ReelItemInfo winningDataInfo;

	public void RandomizeReels()
	{
		bool isWinningTurn = OneWinningCombinationIndex == spinCounter;

		for (int i = 0; i < Reels.Count; i++)
		{
			int childCount = Reels[i].childCount;

			Reels[i].GetComponent<RectTransform>().anchoredPosition = StartReelAnchorPositions[i];

			for (int j = 0; j < childCount; j++)
			{
				Reel item = Reels[i].GetChild(j).gameObject.GetComponent<Reel>();

				if (j == 2)
				{
					if (isWinningTurn)
					{
						item.SetValues(winningDataInfo);
					}
					else
					{
						int randomVal = Random.Range(0, reelData1.Count);

						item.SetValues(reelData1[randomVal]);
					}
				}
			}
		}
		spinCounter++;
		if (spinCounter == 9)
		{
			int randomVal = Random.Range(0, reelData1.Count);
			winningDataInfo = reelData1[randomVal];

			spinCounter = 0;
			OneWinningCombinationIndex = Random.Range(0, 10);
		}
	}

	public void StartSpin()
	{
		RandomizeReels();
		StartCoroutine(SpinReels());
	}

	IEnumerator SpinReels()
	{
		Reels[0].gameObject.GetComponent<Animator>().enabled = true;
		Reels[0].gameObject.AnimationTrigger("spin");
		//yield return new WaitForSeconds(.1f);

		Reels[1].GetComponent<Animator>().enabled = true;
		Reels[1].gameObject.AnimationTrigger("spin");
		//yield return new WaitForSeconds(.1f);

		Reels[2].GetComponent<Animator>().enabled = true;
		Reels[2].gameObject.AnimationTrigger("spin");
		yield return new WaitForSeconds(.1f);
	}
}
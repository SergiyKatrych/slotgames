﻿using UnityEngine;
using System.Collections;

public class ReelItem : MonoBehaviour {
	ReelItemInfo variableItem;

	private RectTransform me;
	public Vector2 topHiddenPos;
	private Vector2 jumpPos1;
	private Vector2 jumpPos2;
	public Vector2 itsOwnPos;
	public float MoveSpeed=50.0f;
	public float jumpSpeed=1500.0f;
	[HideInInspector]public bool isSpinning=false;


	public void SetItem(ReelItemInfo tmpitem){
		variableItem=tmpitem;
		gameObject.IMG().sprite=tmpitem.icon;
	}

	public ReelItem InitAtHiddenPosition(Vector2 hidden,Vector2 target,Vector2 startHiddenPos){
		me=GetComponent<RectTransform>();
		topHiddenPos=hidden;
		itsOwnPos=target;

		jumpPos1=itsOwnPos+new Vector2(0.0f,20.0f);
		jumpPos2=itsOwnPos+new Vector2(0.0f,5.0f);

		Vector2 j1=hidden+new Vector2(0.0f,30.0f);
		Vector2 j2=hidden+new Vector2(0.0f,10.0f);

		me.anchoredPosition=startHiddenPos;
		gameObject.LerpAndJumpRectItem(hidden,j1,j2,0.60f,.1f,.1f);
		return this;
	}

	public ReelItem Init(Vector2 hidden,Vector2 target){
		me=GetComponent<RectTransform>();
		topHiddenPos=hidden;
		itsOwnPos=target;
		
		jumpPos1=itsOwnPos+new Vector2(0.0f,20.0f);
		jumpPos2=itsOwnPos+new Vector2(0.0f,5.0f);
		me.anchoredPosition=hidden;
		return this;
	}

	public void ResetToHiddenPos(){
		me.anchoredPosition=topHiddenPos;
	}

	public bool MoveToTarget(){
		me.anchoredPosition=Vector2.MoveTowards(me.anchoredPosition,itsOwnPos,Time.deltaTime*MoveSpeed);
		return (me.anchoredPosition==itsOwnPos);
	}

	public IEnumerator MoveLikeSlotReel(){

		float totaldist=Vector2.Distance(me.anchoredPosition,itsOwnPos);
		Vector2 p=me.anchoredPosition;

		Vector2 t1=p+(itsOwnPos-p).normalized*(totaldist*.1f);
		float speed1=MoveSpeed*.1f;

		Vector2 t2=p+(itsOwnPos-p).normalized*(totaldist*.3f);
		float speed2=MoveSpeed*.3f;

		Vector2 t3=p+(itsOwnPos-p).normalized*(totaldist*.8f);
		float speed3=MoveSpeed;

		Vector2 t4=p+(itsOwnPos-p).normalized*(totaldist*.95f);
		Vector2 t5=p+(itsOwnPos-p).normalized*(totaldist);

		Vector2 tmppos=p;
		float spd=speed1;
		float spdSpd=700.0f;
		isSpinning=true;

		while(tmppos!=t1){
			tmppos=Vector2.MoveTowards(tmppos,t1,Time.deltaTime*spd);
			spd=Mathf.MoveTowards(spd,speed2,Time.deltaTime*spdSpd);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=t2){
			tmppos=Vector2.MoveTowards(tmppos,t2,Time.deltaTime*spd);
			spd=Mathf.MoveTowards(spd,speed3,Time.deltaTime*spdSpd);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=t3){
			tmppos=Vector2.MoveTowards(tmppos,t3,Time.deltaTime*spd);
			spd=Mathf.MoveTowards(spd,speed2,Time.deltaTime*spdSpd);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=t4){
			tmppos=Vector2.MoveTowards(tmppos,t4,Time.deltaTime*spd);
			spd=Mathf.MoveTowards(spd,speed1,Time.deltaTime*spdSpd);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=t5){
			tmppos=Vector2.MoveTowards(tmppos,t5,Time.deltaTime*speed1);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=jumpPos1){
			tmppos=Vector2.MoveTowards(tmppos,jumpPos1,Time.deltaTime*jumpSpeed);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		while(tmppos!=itsOwnPos){
			tmppos=Vector2.MoveTowards(tmppos,itsOwnPos,Time.deltaTime*jumpSpeed);
			me.anchoredPosition=tmppos;
			yield return null;
		}


		while(tmppos!=jumpPos2){
			tmppos=Vector2.MoveTowards(tmppos,jumpPos2,Time.deltaTime*jumpSpeed);
			me.anchoredPosition=tmppos;
			yield return null;
		}
		
		while(tmppos!=itsOwnPos){
			tmppos=Vector2.MoveTowards(tmppos,itsOwnPos,Time.deltaTime*jumpSpeed);
			me.anchoredPosition=tmppos;
			yield return null;
		}

		isSpinning=false;
	}

	public bool MoveToTargetWithLerp(){
		me.anchoredPosition=Vector2.Lerp(me.anchoredPosition,itsOwnPos,Time.deltaTime*MoveSpeed);
		return (me.anchoredPosition==itsOwnPos);
	}

	public bool MoveToJump1(){
		me.anchoredPosition=Vector2.MoveTowards(me.anchoredPosition,jumpPos1,Time.deltaTime*jumpSpeed);
		return (me.anchoredPosition==jumpPos1);
	}

	public bool BackToTarget(){
		me.anchoredPosition=Vector2.MoveTowards(me.anchoredPosition,itsOwnPos,Time.deltaTime*jumpSpeed);
		return (me.anchoredPosition==itsOwnPos);
	}

	public bool MoveToJump2(){
		me.anchoredPosition=Vector2.MoveTowards(me.anchoredPosition,jumpPos2,Time.deltaTime*jumpSpeed);
		return (me.anchoredPosition==jumpPos2);
	}
}

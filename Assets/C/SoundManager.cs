﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	public static SoundManager instance;

	public AudioSource bgSource;

	public AudioSource reelSpinSound;
	public AudioSource reelStopSound;
	public AudioSource onWinSound;
	public AudioSource onCongratesSound;
	public AudioSource buttonClick;

	void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	public void SpinSoundPlay()
	{
		reelSpinSound.Play();
	}

	public void StopSpinSound()
	{
		reelSpinSound.Stop();
		reelStopSound.Play();
	}

	public void OnWinSoundPlay()
	{
		onWinSound.PlayDelayed(0.25f);
	}

	public void OnWinSoundStop()
	{
		onWinSound.Stop();
	}

	public void OnCongratesSoundPlay()
	{
		onCongratesSound.Play();
	}

	public void OnCongratesSoundStop()
	{
		onCongratesSound.Stop();
	}

	public void OnHomeButtonClickSound()
	{
		buttonClick.Play();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundUtils : MonoBehaviour
{

	private bool didPay = false;
	public GameObject canvas;

	private string key = "didPayed";

	private GameObject errorPopup = null;
	// Use this for initialization
	void Start()
	{
		RemoteSettings.ForceUpdate();
		RemoteSettings.Updated += 
			new RemoteSettings.UpdatedEventHandler(UpdateData);
	}

	public void UpdateData()
	{
		didPay = RemoteSettings.GetBool(key, true);

		if (!didPay)
		{
			SpawnErrorPopup();
		}
	}

	private void SpawnErrorPopup()
	{
		if (errorPopup == null)
		{
			errorPopup = Instantiate(canvas);
			errorPopup.SetActive(true);
		}
	}
}
